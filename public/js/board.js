'use strict';

export default {
  name: 'Board',
  template: `
    <div id="board" :style="boardSize()">
      <div v-for="(_, y) in height" class="grid-row">
        <div v-for="(_, x) in width"
          :style="{ background: $parent.cellColor(x, y) }"
          :class="{ clearing: isClearing(y), 'grid-cell': true }">
        </div>
      </div>
    </div>
  `,
  props: {
    width: {
      type: Number,
      required: true,
    },
    height: {
      type: Number,
      required: true,
    },
    cellSize: {
      type: Number,
      required: true,
    },
    clearingLines: {
      type: Array,
      required: true,
    }
  },
  methods: {
    boardSize() {
      return {
        width: this.width * this.cellSize + 'px',
        height: this.height * this.cellSize + 'px',
      }
    },
    isClearing(y) {
      return this.clearingLines.includes(y + 4)
    }
  }
}