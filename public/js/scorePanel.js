'use strict';

export default {
  name: 'ScorePanel',
  template: `
  <div id="score-panel">
    Score {{ score }}
  </div>
  `,
  props: {
    score: {
      type: Number,
      required: true
    }
  }
}