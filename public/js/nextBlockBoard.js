'use strict';

export default {
  name: 'NextBlockBoard',
  template: `
    <div id="next-block-board" :style="boardSize()">
      <div v-for="(_, y) in 4" class="grid-row">
        <div v-for="(_, x) in 4" class="grid-cell"
          :style="{ background: $parent.nextBlockCellColor(x, y) }">
        </div>
      </div>
    </div>
  `,
  props: {
    cellSize: {
      type: Number,
      required: true,
    }
  },
  methods: {
    boardSize() {
      return {
        width: 4 * this.cellSize + 'px',
        height: 4 * this.cellSize + 'px',
      }
    }
  }
}