'use strict';

export default {
  name: 'StartPanel',
  template: `
  <div id="start-panel">
    <h1>{{ title }}</h1>
  </div>
  `,
  props: {
    title: {
      type: String,
      required: true
    }
  }
}