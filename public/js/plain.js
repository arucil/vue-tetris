'use strict';

import Vue from 'https://unpkg.com/vue@2.6.10/dist/vue.esm.browser.min.js'
import Board from "./board.js"
import NextBlockBoard from "./nextBlockBoard.js"
import StartPanel from "./startPanel.js"
import ScorePanel from "./scorePanel.js"

const COLOR_WHEEL = [0, 30, 60, 90, 120, 150, 180, 210, 240, 270, 300, 330]
const WIDTH = 10
const HEIGHT = 18
const CELL_SIZE = 20
const ROTATE_TABLE = [
  0,
  2, 3, 4, 1,
  6, 7, 8, 5,
  18,
  11, 10,
  13, 12,
  15, 16, 17, 14,
  9
]
const BLOCK_OFFSET_Y_TABLE = [
  -1, 0, -1, 0, -1, 0,
  -1, 0, -1, 0, -1, 0,
  -1, 0, -1, 0, -1, 0,
  -1
]
const BLOCK_CELLS_TABLE = [
  [ [1,2], [2,2], [1,3], [2,3] ],
  [ [1,1], [1,2], [1,3], [2,3] ],
  [ [1,2], [1,3], [2,2], [3,2] ],
  [ [1,1], [2,1], [2,2], [2,3] ], // 3
  [ [0,3], [1,3], [2,3], [2,2] ],
  [ [1,3], [2,1], [2,2], [2,3] ],
  [ [1,2], [1,3], [2,3], [3,3] ],
  [ [1,1], [1,2], [1,3], [2,1] ], // 7
  [ [0,2], [1,2], [2,2], [2,3] ],
  [ [2,0], [2,1], [2,2], [2,3] ],
  [ [1,3], [2,2], [2,3], [3,2] ],
  [ [1,1], [1,2], [2,2], [2,3] ], // 11
  [ [0,2], [1,2], [1,3], [2,3] ],
  [ [1,2], [1,3], [2,1], [2,2] ],
  [ [1,3], [2,2], [2,3], [3,3] ],
  [ [1,1], [1,2], [1,3], [2,2] ], // 15
  [ [1,2], [2,2], [3,2], [2,3] ],
  [ [1,2], [2,1], [2,2], [2,3] ],
  [ [0,3], [1,3], [2,3], [3,3] ], // 18
]
const BLOCK_HEIGHT_TABLE = [
  2, 3, 2, 3, 2, 3,
  2, 3, 2, 4, 2, 3,
  2, 3, 2, 3, 2, 3,
  1
]
const SCORE_TABLE = [0, 1, 3, 5, 8]

// 0           1           2           3           4            5
//  . . . .     . . . .     . . . .     . . . .     . . . .      . . . .
//  . . . .     . * . .     . . . .     . * * .     . . . .      . . * .
//  . * * .     . * . .     . * * *     . . * .     . . * .      . . * .
//  . * * .     . * * .     . * . .     . . * .     * * * .      . * * .

// 6           7           8           9           10           11
//  . . . .     . . . .     . . . .     . . * .     . . . .      . . . .
//  . . . .     . * * .     . . . .     . . * .     . . . .      . * . .
//  . * . .     . * . .     * * * .     . . * .     . . * *      . * * .
//  . * * *     . * . .     . . * .     . . * .     . * * .      . . * .

// 12          13          14          15          16           17
//  . . . .     . . . .     . . . .     . . . .     . . . .      . . . .
//  . . . .     . . * .     . . . .     . * . .     . . . .      . . * .
//  * * . .     . * * .     . . * .     . * * .     . * * *      . * * .
//  . * * .     . * . .     . * * *     . * . .     . . * .      . . * .

// 18      
//  . . . .
//  . . . .
//  . . . .
//  * * * *

// 生成2个新方块
// 状态：初始界面，结束界面，下落，消除
// 下落:
//   判断是否碰撞？是，结束游戏
//   左右、旋转（判断是否碰撞、左右边界）

new Vue({
  el: '#app',
  data: {
    width: WIDTH,
    height: HEIGHT,
    cellSize: CELL_SIZE,
    board: Array(HEIGHT + 4).fill(1).map(() =>
      Array(WIDTH).fill(1).map(() => ({
        color: null
      }))),
    nextBlockBoard: Array(4).fill(1).map(() =>
      Array(4).fill(1).map(() => ({
        color: null
      }))),
    block: 0,
    color: 0,
    x: 0,
    y: 0,
    nextBlock: undefined,
    nextColor: 0,
    state: 'ready', // clearing, falling, gameover, paused
    keys: new Set(),
    score: 0,
    lastTime: 0,
    clearingDuration: 0,
    lastStepTime: 0,
    clearingLines: [],
  },
  methods: {
    cellColor(x, y) {
      const color = this.board[y + 4][x].color
      if (color === null) {
        return undefined
      } else {
        return `hsl(${COLOR_WHEEL[color]}, 80%, 70%)`
      }
    },

    nextBlockCellColor(x, y) {
      const color = this.nextBlockBoard[y][x].color
      if (color === null) {
        return undefined
      } else {
        return `hsl(${COLOR_WHEEL[color]}, 80%, 70%)`
      }
    },

    genNextBlock() {
      this.block = this.nextBlock
      this.color = this.nextColor

      if (this.nextBlock !== undefined) {
        this.clearBlock(this.nextBlockBoard, this.nextBlock,
          0, BLOCK_OFFSET_Y_TABLE[this.nextBlock])
      }

      this.nextColor = Math.random() * COLOR_WHEEL.length | 0
      this.nextBlock = Math.random() * ROTATE_TABLE.length | 0

      this.drawBlock(this.nextBlockBoard, this.nextBlock,
        0, BLOCK_OFFSET_Y_TABLE[this.nextBlock], this.nextColor)
      
      // 方块开始下落
      if (this.block !== undefined) {
        this.y = 0
        this.x = Math.floor(this.width / 2) - 2
        this.drawBlock(this.board, this.block, this.x, this.y, this.color)
      }
    },

    drawBlock(board, block, x, y, color) {
      this.applyBlock(board, block, x, y, (x, y, cell) => {
        cell.color = color
      })
    },

    clearBlock(board, block, x, y) {
      this.applyBlock(board, block, x, y, (x, y, cell) => {
        cell.color = null
      })
    },

    applyBlock(board, block, x, y, action) {
      const fn = (dx, dy) => {
        const tx = x + dx
        const ty = y + dy
        const cell = tx >= 0 && tx < board[0].length &&
          ty >= 0 && ty < this.board.length ?
          board[ty][tx] :
          undefined
        return action(x + dx, y + dy, cell)
      }
      const cells = BLOCK_CELLS_TABLE[block]

      return fn(cells[0][0], cells[0][1]) ||
        fn(cells[1][0], cells[1][1]) ||
        fn(cells[2][0], cells[2][1]) ||
        fn(cells[3][0], cells[3][1])
    },

    startGame() {
      this.board.forEach(row =>
        row.forEach(cell =>
          cell.color = null))

      this.nextBlockBoard.forEach(row =>
        row.forEach(cell =>
          cell.color = null))

      this.nextBlock = undefined

      this.genNextBlock()
      this.genNextBlock()
      this.score = 0
      this.state = 'falling'

      this.lastTime = +new Date()
      this.lastStepTime = 0
      requestAnimationFrame(this.gameFrame.bind(this))
    },

    handleKeyDown(key) {
      if (this.state === 'ready' || this.state === 'gameover') {
        if (key === 'Enter') {
          this.startGame()
        }
      } else if (key === 'p') {
        if (this.state === 'paused') {
          this.state = 'falling'
        } else if (this.state === 'falling') {
          this.state = 'paused'
        }
      } else {
        this.keys.add(key)
      }
    },

    handleKeyUp(key) {
      if (key === 'ArrowDown') {
        this.keys.delete(key)
      }
    },

    gameFrame(time) {
      if (this.state === 'falling' || this.state === 'clearing') {
        this.update(time / 1e3, (time - this.lastTime) / 1e3)
      }

      this.lastTime = time

      if (this.state !== 'gameover') {
        requestAnimationFrame(this.gameFrame.bind(this))
      }
    },

    update(time, delta) {
      // 消除的动画
      if (this.state === 'clearing') {
        this.clearingDuration += delta
        if (this.clearingDuration >= 0.1) {
          this.state = 'falling'

          // 清除行
          let newY = this.height + 3
          for (let y = this.height + 3; y >= 0; --y) {
            this.board[newY].forEach((cell, i) => {
              cell.color = this.board[y][i].color
            })
            if (!this.clearingLines.includes(y)) {
              --newY
            }
          }

          // 填补行
          for (; newY >= 0; --newY) {
            this.board[newY].forEach(cell => cell.color = null)
          }

          this.clearingLines = []

          this.genNextBlock()
        }
        return
      }
    
      // 左右
      if (this.keys.has('ArrowLeft')) {
        this.move(-1, 0)
      } else if (this.keys.has('ArrowRight')) {
        this.move(1, 0)
      }
      this.keys.delete('ArrowLeft')
      this.keys.delete('ArrowRight')

      // 旋转
      if (this.keys.has('ArrowUp')) {
        this.rotate()
      }
      this.keys.delete('ArrowUp')

      // 间隔一段时间再下落
      const step = this.keys.has('ArrowDown') ? 0.03 : 0.7

      if (time - this.lastStepTime < step) {
        return
      }

      this.lastStepTime = time

      if (!this.move(0, 1)) {
        if (this.y < BLOCK_HEIGHT_TABLE[this.block]) {
          this.state = 'gameover'
          return
        }

        // 消除
        for (let y = this.y + 4 - BLOCK_HEIGHT_TABLE[this.block]; y < this.y + 4; ++y) {
          if (this.board[y].every(cell => cell.color !== null)) {
            this.clearingLines.push(y)
          }
        }

        this.score += SCORE_TABLE[this.clearingLines.length]

        // 下一个方块会立即下落
        this.lastStepTime = 0

        if (!this.clearingLines.length) {
          this.genNextBlock()
        } else {
          this.state = 'clearing'
          this.clearingDuration = 0
        }
      }
    },

    move(dx, dy) {
      this.clearBlock(this.board, this.block, this.x, this.y)
      let canMove = !this.collide(this.block, this.x + dx, this.y + dy)
      if (canMove) {
        this.x += dx
        this.y += dy
      }
      this.drawBlock(this.board, this.block, this.x, this.y, this.color)
      return canMove
    },

    rotate() {
      this.clearBlock(this.board, this.block, this.x, this.y)

      const rotated = ROTATE_TABLE[this.block]
      const collided = this.collide(rotated, this.x, this.y)
      if (typeof collided === 'number') {
        if (collided < 0) {
          this.block = rotated
          this.x -= collided
        } else {
          this.block = rotated
          this.x -= collided
        }
      } else if (!collided) {
        this.block = rotated
      }

      this.drawBlock(this.board, this.block, this.x, this.y, this.color)
    },

    /* returns:
     *   false              jnot collided
     *   negative integer   exceeded left boundary
     *   positive integer   exceeded right boundary
     *   true               collided
     */
    collide(block, x, y) {
      let collided = 0
      const ret = this.applyBlock(this.board, block, x, y, (x, y, cell) => {
        if (cell && cell.color !== null) {
          return true
        }

        if (y >= this.height + 4) {
          return true
        }

        if (x < 0) {
          if (x < collided) {
            collided = x
          }
        } else if (x >= this.width) {
          if (x - this.width + 1 > collided) {
            collided = x - this.width + 1
          }
        }

        return false
      })

      return ret ? ret : collided ? collided : false
    }
  },

  mounted() {
    window.addEventListener('keydown', event => {
      this.handleKeyDown(event.key)
    })
    window.addEventListener('keyup', event => {
      this.handleKeyUp(event.key)
    })
  },

  computed: {
    title() {
      return this.state === 'ready' ? 'Press Enter' :
        this.state === 'paused' ? 'Paused' :
        this.state === 'gameover' ? 'Score ' + this.score :
        ''
    }
  },

  components: {
    Board,
    NextBlockBoard,
    StartPanel,
    ScorePanel
  }
})